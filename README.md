My Résumé
=========

I was tired of keeping my résumé in a cloud storage location since I use
LaTeX to compile it and keep it beautifully formatted. As such, I've decided
to do the logical thing and keep the text format versioned in a Git repository.

### Building

Build the document with a docker container:

    docker run -it --rm -v "${PWD}":/workspace:z -w /workspace aergus/latex make

