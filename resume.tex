\documentclass{article}

%% Preamble
\usepackage[letterpaper, margin=0.5in]{geometry}  % Enforce 0.5 inch margins
\usepackage{marvosym}                             % Special symbols
\usepackage{multicol}                             % Multicolumn lists
\usepackage{fontawesome}                          % Specialer symbols
\usepackage{sectsty}                              % Section font styling
\usepackage{titlesec}
\usepackage{xcolor}

\titleformat{\section}
  {\normalfont\large\bfseries}{\thesection}{1em}{}[{\titlerule[0.5pt]}]
\setlength\parindent{0pt}

\usepackage{tgheros}
\renewcommand*\familydefault{\sfdefault}
\usepackage[T1]{fontenc}

\begin{document}

\pagenumbering{gobble}

%% Resume Header
\fboxsep 8pt
\colorbox{black!80}{\begin{minipage}{0.994\linewidth}
    \textcolor{white}{\huge \textbf{Joshua Ford}}
\end{minipage}}

%% Left column start
\begin{minipage}[t]{0.66\linewidth}

\section*{Work Experience}
\begin{minipage}{1.0\linewidth}
    \centering
    \begin{minipage}{0.95\linewidth}
        \raggedright
        {\textbf{Software AG}},
        Oklahoma City, OK (Remote) \hfill October 2022 -- Present \\
        \textit{DevOps Engineer}
        \begin{itemize}
                \setlength\itemsep{0em}
            \item Growing and maintaining a FedRAMP Moderate-authorized
                environment on AWS GovCloud.
            \item Maintained NIST 800-53 documentation for FedRAMP
                authorization processes, such as SSPs.
            \item Rearchitected an ElasticSearch logging stack in order to
                maintain OMB M-21-31 log retention compliance.
            \item Created an internal Python automation library for
                automating business tasks and customer environments.
            \item Implemented highly available, scalable Thanos (Prometheus),
                Loki clusters for metrics and log gathering.
            \item Utilized Grafana and Alertmanager to monitor environment health.
            \item Created GitHub Actions shared workflows from scratch.
            \item Implemented pipelines for automatically signing containers
                with Cosign.
            \item Used Flux and ArgoCD to perform GitOps deployments to production.
            \item Deployed Argo Workflows to handle team automation tasks.
            \item Automated the deployment of EKS clusters with Make and eksctl.
            \item Created policies for increasing security and reducing cloud spend
                with Cloud Custodian.
        \end{itemize}
    \end{minipage}

    \bigskip

    \begin{minipage}{0.95\linewidth}
        \raggedright
        {\textbf{Boeing}},
        Oklahoma City, OK \hfill December 2017 -- August 2022 \\
        \textit{Software Engineer}
        \begin{itemize}
                \setlength\itemsep{0em}
            \item Introduced Jenkins CI to the software engineering environment.
            \item Built and maintained mapping software written in C++.
            \item Established automated unit test repositories for C++ and C\# codebases.
            \item Took on team Release Engineer role and handled deployments.
            \item Led the rollout of Atlassian tools to the business unit.
            \item Presented slide decks on new technology and methodologies.
            \item Collaborated with cybersecurity teams to gather information,
                harden systems, and present findings to management and the DoD.
            \item Architected a DevSecOps software factory using Terraform,
                Ansible, Kubernetes, and AWS.
            \item Mentored engineers on DevSecOps best practices, such as
                the practices of Continuous Integration and automated testing.
            \item Built AWS cloud environments with Terraform.
            \item Generated software bills of materials with Anchore Syft.
            \item Created Terraform modules and libraries.
            \item Created secure Docker containers with GitLab CI.
            \item Created Helm charts and deployed them to Kubernetes with Flux.
            \item Championed an innersource model within the company.
            \item Deployed air-gapped Kubernetes clusters with Ansible
              and Terraform.
            \item Advised the rollout of DevSecOps practices to other teams.
        \end{itemize}
    \end{minipage}

    \bigskip

\end{minipage}


%% Left column end
\end{minipage}
%% Right column start
\fboxsep 8pt
\colorbox{gray!10}{\begin{minipage}[t][0.9\textheight][t]{0.33\linewidth}

\section*{Personal Info}
\begin{tabular}{ c l }
    \faicon{map-marker}      & Oklahoma City, OK \\
    \faicon{envelope-o}      & joshua.ford@protonmail.com \\
    \faicon{linkedin-square} & linkedin.com/in/joshua-a-ford
\end{tabular}

\bigskip

\section*{Soft Skills}
\begin{itemize}
        \setlength\itemsep{0em}
    \item Self-directed
    \item Effective communicator
    \item Empathetic
    \item Desire to learn
    \item Desire to teach
\end{itemize}

\section*{Technical Skills}
\textsl{Languages}
\begin{itemize}
        \setlength\itemsep{0em}
    \item Shell scripting
    \item C \& C++
    \item C\#
    \item Java
    \item Perl
    \item Python
    \item SQL
\end{itemize}

\textsl{Tools}
\begin{itemize}
        \setlength\itemsep{0em}
    \item Ansible
    \item AWS
    \item Docker
    \item Git
    \item GitLab \& GitLab CI
    \item Helm
    \item Jenkins
    \item Kubernetes
\end{itemize}

\textsl{Operating Systems}
\begin{itemize}
        \setlength\itemsep{0em}
    \item CentOS, Fedora, \& RHEL
    \item Debian \& Ubuntu
\end{itemize}

\textsl{Methodologies}
\begin{itemize}
        \setlength\itemsep{0em}
    \item Agile Software Development
    \item DevSecOps
    \item Scaled Agile Framework
\end{itemize}

%% Right column end
\end{minipage}}

%% PAGE 2 start
\begin{minipage}[t]{0.95\linewidth}

%% Work experience, cont.
\begin{minipage}{0.95\linewidth}

    \raggedright
    {\textbf{Paycom}},
    Oklahoma City, OK \hfill February 2017 -- November 2017 \\
    \textit{Software Developer}
    \begin{itemize}
            \setlength\itemsep{0em}
        \item Maintained the primary PHP- and C\#-based SaaS app.
        \item Helped break down the communication barriers between teams.
        \item Extended the feature set of the SaaS app with vendor REST APIs.
        \item Mentored other developers on software design patterns.
        \item Leveraged PHP Code Sniffer and Mess Detector to locate code defects.
    \end{itemize}

    \bigskip

    \raggedright
    {\textbf{Seagate Technology}},
    Oklahoma City, OK \hfill July 2013 -- January 2017 \\
    \textit{IT Systems Engineer}
    \begin{itemize}
        \setlength\itemsep{0em}
        \item Automated the data center deployment of OpenStack.
        \item Team lead on project for automating server hardening processes.
        \item Maintained both Puppet Enterprise and FOSS Puppet environments.
        \item Maintained an internal GitLab server for operations staff.
        \item Set up continuous integration processes for internal Puppet modules.
        \item Improved and maintained internal SOX compliance auditing tool.
    \end{itemize}

    \bigskip
\end{minipage}

\section*{Education}
\begin{minipage}{1.0\linewidth}
    \centering
    \begin{minipage}{0.95\linewidth}
        {\textbf{University of Central Oklahoma}},
        Edmond, OK \hfill  May 2013 \\
        Bachelor of Science in Computer Science, Minor in Mathematics \\
        Graduated Cum Laude \\
        GPA: 3.71
    \end{minipage}
\end{minipage}

%% PAGE 2 end
\end{minipage}

\end{document}
